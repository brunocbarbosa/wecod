import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class UserListService {

  private readonly api = 'https://randomuser.me/api/?results=20&inc=name,email,phone,id,picture&noinfo';

  constructor(private http:HttpClient) { }

  getUsers(){
    return this.http.get(this.api)
      .pipe(
        tap(console.log)
      );
  }

  getUserById(id: string){
    return this.http.get(`${this.api}/detail/${id}`)
  }
}
