import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router'
import { UserListService } from '../user-list/user-list.service'
import { UserListComponent } from '../user-list/user-list.component';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  userListComponents: UserListComponent

  constructor(private userListService: UserListService, private route: ActivatedRoute) { }

  ngOnInit() {
    // this.userListService.getUserById(this.route.snapshot.params['id'])
    //   .subscribe(userListComponents => this.userListComponents = userListComponents)
  }

}
